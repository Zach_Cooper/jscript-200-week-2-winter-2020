//ASSIGNMENT FOR CLASS #2

//Assignment Date Problem (First Class Review) (Links to an external site.)
//7. You are given an assignmentDate as a string in the format 
//"month/day/year", i.e. '1/21/2019'.  Convert this string to a Date
const stringDate= Date.parse('1/21/2019');
const assignmentDate = new Date(stringDate); 
console.log(`The original assignment date is ${assignmentDate}`);



//8. Create a new Date instance to represent the dueDate.  
//This will be exactly 7 days after the assignment date.

const dueDate = new Date(assignmentDate.setDate(28));
console.log(`The new assignment date is ${dueDate}`);

//or
//.getMonth returns 0-11 so we can use an array for string values at each month index
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];
const day = assignmentDate.getDate()+7;
const year = assignmentDate.getFullYear();
let dueDate = (`${months[0]} ${day}, ${year}`);
console.log(dueDate);


//9. Use dueDate values to create an HTML time tag in format:

//'<time datetime="2018-01-14">January 14, 2018</time>'
//const months = [
//    'January',
//   'February',
//    'March',
//    'April',
//    'May',
//    'June',
//    'July',
//    'August',
//    'September',
//    'October',
//    'November',
//    'December'
//  ];
  const dateFriendlyTime = (`${months[0]} ${day}, ${year}`);
  //January 14, 2018
  //console.log(dateFriendlyTime);
  //'2018-01-14'
  const monthsPad = '1'
  const datetimeAttributeTime = (`${year}-${monthsPad.padStart(2,'0')}-${day}`);
  //console.log(datetimeAttributeTime);



//10. Log this to the console.
//January 14, 2018
console.log(dateFriendlyTime);
//'2018-01-14'
console.log(datetimeAttributeTime);