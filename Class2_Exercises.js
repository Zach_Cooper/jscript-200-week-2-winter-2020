//1. Create an object representation of yourself.  Should include:
//firstName
//lastName
//'favorite food'
//bestFriend (object with the same 3 properties as above)
const person = {
  firstName: 'Zach',
  lastName: 'Cooper',
  'favorite food': 'Pizza',
  bestFriend:{
      firstName: 'Chris',
      lastName: 'Orechia',
      'favorite food': 'Mexican',
  }
};

//or

const bestFriend = {
  firstName: 'Chris',
  lastName: 'Orechia',
  'favorite food': 'Mexican',
};

const person = {
    firstName: 'Zach',
    lastName: 'Cooper',
    'favorite food': 'Pizza',
    bestFriend,
  };

//2. console.log best friend's firstName and your favorite food
console.log(`My bestfriends first name is ${person.bestFriend.firstName}`);
console.log(`His favorite food is ${person['favorite food']}`);


//3. Create an array  to represent this tic-tac-toe board
const ticTacToe = [['-','O','-'],['-','X','O'],['X','-','X']];

console.log(`TicTacToe Board
    ${ticTacToe[0]},
    ${ticTacToe[1]},
    ${ticTacToe[2]}`
);

//4. After the array is created, O claims the top right square. Update that value.
ticTacToe[0][2] = 'O';

//5. Log the grid to the console.
console.log(`TicTacToe Board Updated
    ${ticTacToe[0]},
    ${ticTacToe[1]},
    ${ticTacToe[2]}`
);

//6. You are given an email as string myEmail, make sure it is in correct email format. 
//Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, 
//then one or more characters - no whitespace.  
//i.e. "foo@bar.baz" is a good email, "brett@ mall" is not.
const regex = /\w+[@]\w+[\.*]\w+/;

const myEmail = 'zcoop723@gmail.com';

console.log(regex.test(myEmail));
